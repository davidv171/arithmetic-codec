package um.feri;

import java.util.ArrayList;

/*1. calculate probability table
 * 2. initialize on 4 quarters
 * 3. code every symbol in the same order its s
 *   3.1 calculate new borders for intervals
 *   3.2 check errors, based on which output bits are picked*/
class ArithmeticCoder {
    //Size of the alphabet, if if we have 1 bytes, that means our alphabet size is 256 different bit combinations
    int alphabetSize;
    //2^32
    //final long upperLimit = 4294967295L;
    final long upperLimit = 127;
    int uniqueSymbolsCount = 0;
    long fileSize = 0;
    //How many bits we're encoding, the assignment asks for a 32 bit encoder
    //The probability array of every single byte
    int[] freqTable;
    int[] highTable;
    int[] lowTable;
    long high = upperLimit;
    long low = 0;
    long step = 0;

    ArithmeticCoder(int alphabetSize, long fileSize) {
        this.alphabetSize = alphabetSize;
        this.freqTable = new int[alphabetSize];
        this.highTable = new int[alphabetSize];
        this.lowTable = new int[alphabetSize];
        this.fileSize = fileSize;
        this.high = upperLimit;
        this.low = 0;
        this.step = (this.high - this.low + 1) / fileSize;
    }

    //TEST: Sum the probabilities up, compare to size of read file
    //package private
    //Calculate probabilities of each symbol
    //The symbols are almost always 8 bits aka 1 byte long
    //TODO: Add buffer to byteProbabilities calculations
    void byteProbabilities(ArrayList<Integer> bytes) {
        //Initialize a n length array full of zeroes
        //Previous frequency, used to calculate the table of highs
        System.out.println("Building up the model...");
        for (int i = 0; i < bytes.size(); i++) {
            if (this.freqTable[bytes.get(i)] == 0) {
                this.uniqueSymbolsCount++;
            }
            //One dimensional table, that keeps track of how many times each element shows up in a file
            //The index of the array/table is also the value, so index 52 having the value of 500 means that the element 52 repeated 52 times
            freqTable[bytes.get(i)]++;
        }
        //Create a 1D table showing the upper limit of an interval for each symbol
        //Symbol is denoted by its index, the value of the index is the higher limit of an interval
        //The first high is equal to the frequency of the first read byte
        this.highTable[bytes.get(0)] = this.freqTable[bytes.get(0)];
        this.lowTable[0] = 0;
        for (int i = 1; i < bytes.size(); i++) {
            if (this.highTable[bytes.get(i)] == 0) {
                this.highTable[bytes.get(i)] = this.highTable[bytes.get(i - 1)] + this.freqTable[bytes.get(i)];
                this.lowTable[bytes.get(i)] = this.highTable[bytes.get(i)] - this.freqTable[bytes.get(i)];
            }
        }
        //calculate intervals
        intervals(bytes);
    }

    //Calculate intervals for everything
    private void intervals(ArrayList<Integer> bytes) {
        //Intervals initialized in constructor!
        //Cumulative sum of every previous symbol until this one
        System.out.println("First step calculated: " + step);
        //Integer to keep track on which byte we're at currently, start at 1 because 0 has different calculations
        //The first high interval value for a certain symbol is equal to the count of the first read symbol
        System.out.println("Freq table first element count" + (freqTable[bytes.get(0)] - 1));
        System.out.println("First high " + high);
        long[] intervals = quarterize();
        int e3_counter = 0;
        //TODO: Figure out how to do this incrementally without loading the entire file into arraylists
        for (int i = 0; i < bytes.size(); i++) {
            //Calculate number of all input unique input symbols
            //32 bit
            // A size 4 array containing quarters, which are later used for interval calculations modifications

            //step = high-low+1 /n
            //high = low + step ? high(symbol) -1
            //low = low + step * low(symbol)
            //n = amount of all input signs
            this.step = (high - low + 1) / this.fileSize;
            //high(symbol) == cumulative high of previous symbol + frequency of current one
            this.high = low + step * (this.highTable[bytes.get(i)]) - 1;
            this.low = low + step * this.lowTable[bytes.get(i)];
            //We have yet to add a cummulative sum to this symbol, so we have to do so
            //Check for errors: E1 if: HIGH < 2nd quarter -> low=low*2, HIGH = high*2 +1,output 0, E3_COUNTER * bit 1, then E3_counter = 0
            while (this.high < intervals[1] || (this.low >= intervals[1])) {
                if (high < intervals[1]) {
                    this.low = this.low * 2;
                    this.high = this.high * 2 + 1;
                    //System.out.print("0");
                    for (int j = 0; j < e3_counter; j++) {
                        //System.out.print("1 ");
                        e3_counter = 0;
                    }
                }
                //E2 if LOW >= 2nd quarter: LOW = 2*(low - 2ndquarter), HIGH = 2*(high - 2ndquarter)+1, OUTPUT: bit 1 and E3_COUNTER * bit 0, then E3_counter = 0;
                else if (this.low >= intervals[1]) {
                    this.low = 2 * (this.low - intervals[1]);
                    this.high = 2 * (this.high - intervals[1]) + 1;
                    //System.out.print("1");
                    for (int j = 0; j < e3_counter; j++) {
                        //System.out.print("0");
                    }
                    e3_counter = 0;
                }
            }
            //System.out.println("");
            while ((intervals[0] <= this.low) && (this.high < intervals[2])) {
                //E3: LOW >= firstQuarter && HIGH < thirdQuarter: LOW = 2 * (LOW - firstQuarter),high = 2*(high - firstQuarter)+1
                if (this.low >= intervals[0]) {
                    this.low = 2 * (this.low - intervals[0]);
                    this.high = 2 * (this.high - intervals[0]) + 1;
                    e3_counter++;
                }
            }


        }
        //What we do with the rest:
        //if spMeja  < firstQuarter : send 01 and E3_Counter times 1, otherwise send 10 and E3_Counter times bit 0
        if (low < intervals[0]) {
            System.out.print(" 01 ");
            for (int i = 0; i < e3_counter; i++) {
                System.out.print("1");
            }
        } else {
            System.out.print(" 10 ");
            for (int i = 0; i < e3_counter; i++) {
                System.out.print("0");
            }
        }

    }

    //Divide the interval from 0 to 2^32 into 4 quarters
    //Each number is the upper value of the
    private long[] quarterize() {
        //We will be exclusively making a 32 bit arithmetic encoder
        //4th quarter: 2^32
        //3rd quarter: 2^32 * 3/4
        //2nd quarter: 2^32 * 1/2
        //1st quarter: 2^32 * 1/4
        long[] quarters = new long[4];
        quarters[0] = upperLimit / 4 + 1;
        quarters[1] = upperLimit / 2 + 1;
        quarters[2] = (upperLimit * 3) / 4 + 1;
        quarters[3] = upperLimit + 1;
        return quarters;
    }
}
