package um.feri;

import java.util.ArrayList;

public class BinaryOperations {
    public BinaryOperations() {

    }

    //The
    ArrayList<Boolean> byteToBits(int currentByte) {
        ArrayList<Boolean> bits = new ArrayList<>();
        for (int i = 7; i >= 0; i--) {
            if (currentByte << ~i < 0) {
                bits.add(true);
            } else {
                bits.add(false);
            }
        }
        return bits;
    }

    byte[] bitsToByte(ArrayList<Boolean> writeArr) throws ArrayIndexOutOfBoundsException {
        byte[] bytes = new byte[writeArr.size() / 8];
        for (int i = 0; i < writeArr.size() / 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (writeArr.get(i * 8 + j)) {
                    bytes[i] |= (128 >> j);
                }

            }
        }
        return bytes;
    }
}
