
package um.feri;

import java.io.*;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) throws IOException {
        // write your code here
        String operation = null, inputFile = null, outputFile = null;
        try {
            operation = args[0];
            inputFile = args[1];
            outputFile = args[2];
        } catch (ArrayIndexOutOfBoundsException err) {
            System.out.println("Missing arguments, ex. java Main c inputfile.jpg outputfile.jpg");
            System.exit(1);
        }
        //Zakodiraj datoteko v izhodno datoteko
        if (operation.equals("c")) {
            //File operations, used for calculating the compression
            //alphabet size is the number of different possible symbols(2^8), since our symbol is a byte
            //fileSize is used to calculate the step, it is the number of all bytes in the file
            File file = new File(inputFile);
            final int alphabetSize = 256;
            long fileSize = file.length();
            ArrayList<Integer> byteArrayList = new ArrayList<>();
            ArithmeticCoder arithmeticCoder = new ArithmeticCoder(alphabetSize, fileSize);
            DataInputStream inputStream = null;
            ArrayList<Boolean> bitArr = null;
            BinaryOperations binaryOperations = new BinaryOperations();
            //Number of bytes we're reading at the same time
            long numberOfBytes = 0;
            try {
                int currentByte;
                bitArr = new ArrayList<>();
                inputStream = new DataInputStream(new FileInputStream(file));
                while (true) {
                    currentByte = inputStream.readUnsignedByte();
                    byteArrayList.add(currentByte);
                    bitArr.addAll(binaryOperations.byteToBits(currentByte));
                    numberOfBytes++;
                    if (numberOfBytes == Integer.MAX_VALUE) {
                        arithmeticCoder.byteProbabilities(byteArrayList);
                        byteArrayList.clear();
                    }
                }

            } catch (EOFException e) {
                //Exception added to method signature!
                //Improves readability, but nothing else
                inputStream.close();
                //Alphabet size is how many different symbols we have, we're dealing with a single byte as our symbol
                arithmeticCoder.byteProbabilities(byteArrayList);
                System.out.println("Done reading file");
                DataOutputStream outputStream = new DataOutputStream(new FileOutputStream(outputFile));
                byte result[] = binaryOperations.bitsToByte(bitArr);
                outputStream.write(result);
                outputStream.close();
                System.out.println("Done writing");
            } catch (FileNotFoundException e) {
                System.out.println("File not found");
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Exception");
            }

        }
    }
}
